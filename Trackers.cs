﻿/*
 * TimeTracker test application
 * Copyright (c) 2020, SailGP, All Rights Reserved
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrackerTest {

    /// <summary>
    /// A Tracker receives reports of remote time reports, along
    /// with the local time at which the report was received.
    /// The calling app can query the Tracker at any time - generally
    /// much more frequently than tie reports come in - to
    /// ask for the predicted remote time given a local time.
    /// </summary>
    public interface ITracker {
        void Set( int id, double local, double remote );
        double GetRemote( double local );
    }


    /// <summary>
    /// Here's a very simple implementation of a Tracker. It remembers
    /// the last remote time it saw, and continually moves an estimated
    /// remote time value toward that remembered value with every call to
    /// GetRemote().  Note this doesn't try to estimate the rate of remote
    /// time change, and will always lag the actual remote time.
    /// </summary>
    public class SimpleTracker : ITracker {
        double last_remote = 0;
        double filtered_remote = 0;
        double new_weight = 0.3;
        bool first = true;

        public double GetRemote( double local ) {
            // Gradually move our filtered estimate toward the last remote value we saw
            filtered_remote = ( 1 - new_weight ) * filtered_remote + new_weight * last_remote;
            return filtered_remote;
        }

        public void Set( int id, double local, double remote ) {
            if ( first ) {
                // Start by completely trusting the first report we receive
                first = false;
                filtered_remote = remote;
            }
            last_remote = remote;
        }
    }

    public class Tracker : ITracker
    {
        double last_remote = 0;
        double last_remote_local_stamp = 0;
        double filtered_remote = 0;
        double new_weight = 0.3;
        double delay_weight = 0;
        bool first = true;

        public double GetRemote(double local)
        {
            DelayDetection(local);

            // Gradually move our filtered estimate toward the last remote value we saw
            //filtered_remote = (1 - new_weight) * filtered_remote + new_weight * last_remote + delay_weight;
            filtered_remote = local + new_weight + delay_weight;
            return filtered_remote;
        }

        public void Set(int id, double local, double remote)
        {
            if (first)
            {
                // Start by completely trusting the first report we receive
                first = false;
                filtered_remote = remote;
                new_weight = remote - local;
            }

            GarbageCollect(remote);

            // store local time stamp against remote message
            last_remote_local_stamp = local;
        }

        private void DelayDetection(double local)
        {
            // compensate if there's a delay 
            // normal interval is 0.1 so if delay is greater then compensate estimate
            // if local is > than 0.1 when first message received then there's a delay
            // or if the local is larger than 0.1 inbetween messages then there's a delay
            if (last_remote != 0)
            {
                if (last_remote_local_stamp > 0.1)
                {
                    // if it's the first time stamp then that is the initial delay
                    if (last_remote_local_stamp == 0.1 && delay_weight == 0)
                    {
                        delay_weight = last_remote_local_stamp;
                    }
                    else
                    {
                        delay_weight = local - last_remote_local_stamp;
                    }
                }
            }
        }

        private void GarbageCollect(double remote)
        {
            // if positive number then use last message to estimate the new message value
            if (remote > 0)
            {
                // if pattern is not being followed then adjust back to expected pattern
                if (remote - last_remote > 0.2)
                {
                    last_remote = last_remote + 0.1;
                }
                else
                {
                    last_remote = remote;
                }
            }
            else
            {
                last_remote = last_remote + 0.1;
            }
        }
    }
}
